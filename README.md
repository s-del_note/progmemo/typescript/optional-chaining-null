# Optional Chaining (Null 条件演算子) の注意点
- [Optional chaining (?.) - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/Optional_chaining)


## 実行環境
- Windows 10 Home (64bit 1909)
    - nvm (1.1.7)
        - Node.js (v10.19.0)
        - npm (6.13.4)
            - [package.json](./package.json)
    - VSCode (1.53.1)


## undefined かもしれない変数に Optional Chaining は使えない
以下の
- 返り値が `null` かもしれない `maybeNull()`
- 返り値が `undefined` かもしれない `maybeVoid()`

の2種類の関数を用意
```ts
/**
 * 返り値が null かもしれない関数
 */
const maybeNull = (): { value: number } | null => {
  const rnd = Math.random();
  return rnd >= 0.5 ? { value: rnd } : null;
};

/**
 * 返り値が undefined かもしれない関数
 */
const maybeVoid = (): { value: number } | void => {
  const rnd = Math.random();
  return rnd >= 0.5 ? { value: rnd } : void 0;
};
```
それぞれの関数の戻り値に Optional Chaining の `?.` で `value` プロパティにアクセスしてみる。
```ts
maybeNull()?.value; // 問題なし
```
```ts
maybeVoid()?.value; // エラー
```
![error](https://gitlab.com/progmemo/typescript/optional-chaining-null/uploads/c1530872cf142631facd220eedd1f415/image.png)


## 理由
- [null - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/null)
    > null 値は null というリテラルです。undefined のようなグローバルオブジェクトのプロパティではありません。
- [undefined - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/undefined)
    > undefined は、グローバルオブジェクトのプロパティです。すなわちグローバルスコープ内の変数です。 undefined の初期値はプリミティブ値である undefined です。

グローバル変数である `undefined` には `value` というプロパティは存在しないため、アクセスしようとするとエラーとなる。


## ありそうなパターン
### アーリーリターンしている関数の返り値に Optional Chaining してしまう
```ts
/**
 * ユーザーID を渡すとユーザー情報か undefined が返される関数
 */
const userInfo = (id: number): UserInfo | void => {
  const user = userMap.get(id); // Map<number, User> 型の変数から get()
  if (!user) return; // ユーザーが存在しない場合アーリーリターン

  return user.info;
};

const name = userInfo(id)?.name; // undefined の可能性があるのでエラー
```
