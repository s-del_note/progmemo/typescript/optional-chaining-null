(() => {
  const maybeNull = (): { value: number } | null => {
    const rnd = Math.random();
    return rnd >= 0.5 ? { value: rnd } : null;
  };

  const maybeVoid = (): { value: number } | void => {
    const rnd = Math.random();
    return rnd >= 0.5 ? { value: rnd } : void 0;
  };

  maybeNull()?.value;
  maybeVoid()?.value;
})();
